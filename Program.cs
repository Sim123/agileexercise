﻿namespace Exercice;

class AppStore
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome to the Elephant Store!");
        Console.WriteLine("Please enter the number of items you wish to purchase");
        int itemsQty = 0;

        int itemPrice = 0;

        var itemCode = "";

        int totalPrice = 0;

        while (itemsQty <= 0)
        {
            try
            {
                itemsQty = Int32.Parse(Console.ReadLine());

                if (itemsQty <= 0)
                {
                    Console.WriteLine("Please enter the number of items you wish to purchase");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid number of items!");
            }
        }

        while (itemsQty > 0)
        {
            try
            {

                Console.WriteLine("Enter the price of the item:");
                itemPrice = Int32.Parse(Console.ReadLine());

                if (itemPrice <= 0)
                {
                    Console.WriteLine("Enter the price of the item:");
                }
                if (itemPrice > 0)
                {
                    totalPrice += itemPrice;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid number of items!");
            }
            itemsQty--;
        }

        Console.WriteLine("Enter the 2-letter state code:");
        itemCode = Console.ReadLine().ToUpper();

        Console.WriteLine("the total is " + totalPrice);

        var taxedTotal = totalPrice + CalculateTaxe(itemCode, totalPrice);

        double discount = taxedTotal - CalculateDiscount(taxedTotal);
        Console.WriteLine("The taxed and discounted price is " + discount);

    }

    private static double CalculateDiscount(double total)
    {
        switch (total)
        {
            case 1000:
                return total * 0.03;
            case 5000:
                return total * 0.05;
            case 7000:
                return total * 0.07;
            case 10000:
                return total * 0.10;
            case 50000:
                return total * 0.15;
        }
        return 0;
    }

    private static double CalculateTaxe(string state, int total)
    {
        switch (state)
        {
            case "UT":
                return total * 0.0685;
            case "NV":
                return total * 0.08;
            case "TX":
                return total * 0.0625;
            case "AL":
                return total * 0.04;
            case "CA":
                return total * 0.0825;
        }
        return total;
    }

}